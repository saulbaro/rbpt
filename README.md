# RBPT

## Prerequisites

* [Git](http://git-scm.com/)
* [Xcode](https://developer.apple.com/xcode/)
* [CocoaPods](http://cocoapods.org/)

## Installation

1. Clone the repo

2. Run `pod install` to install dependencies and use .xcworkspace from now on

3. Run tests -> Command + U

4. Run the app -> Command + R

## Known issues

* If the server operation fails when adding the time to the task, the data will be lost. It won't be fixed due to lack of time, and the fact that the expected situation to fix it isn't clear: should the app show the finished timer and wait for the user to tap Complete? Should the app retry for a number of times and give up?

## Considerations

* I haven't been able to test all the error flows since I can't see the app on my list of "Third Party Apps" in the account (¯\_(ツ)_/¯), so I can't revoke the token in order to check if the app really fails gracefully or not.
 
* There are some rough edges in how the app handles UI updating. I didn't want to use neither RxSwift nor KVO, so I am changing generating a new ViewData struct every time an object changes. This is kind of overkill, even if the user won't see any flickering or visual glitch, it could be done better. The good thing of this approach is that it is simple to follow.

* The tests are very simple, I've done what I could with the given time and my lack of testing experience. I promise I'll get better at this :D

## App architecture

The app has an **AppController** that handles everything, it delegates work to its child coordinators: 

* **AuthenticationCoordinator** to obtain the authorization code and get the Token
* **PomodoroCoordinator** to work with pomodoro sessions

**Each coordinator** presents the ViewController that is needed for each tasks and handles it's interaction via a delegate. The ViewController is responsible for displaying information, and nothing else. Data is only fetched, modified or deleted by the coordinator (or at least it should be this way if I haven't missed any issue ;) ).

**ViewControllers** render **custom views** that inherit from UIView by overriding loadView. These views only expose tappable elements, so the ViewController can setup their actions. Updating the UI data is done via a ViewData struct, that updates the UI elements every time it is set.

To transform from Model objects to ViewData, we have **Presenter** structs. They are the only ones that know how to map model data to view data. Allowing us to change this mapping in the future by replacing these objects.

**Managers** offer the data required by the controllers to do their proper job. They know how the application will work, providing some abstraction from the underlying APIs at the cost of reusability. On the other hand, we could replace the data provider at any point without having to change our UI/Controller logic, simply by providing other manager objects that subscribe to the specific manager protocol.

Managers also have some requirements such as configuration or persistence, that are managed by other objects, allowing us to change some specifics by providing new objects:

* An example of **configuration abstraction** would be the OAuth service details, that are injected when creating the OAuthManager, when the app launches.
* An example of data **persistence abstraction** are the KeychainWrappers, that could be changed at any time by other implementations that fetch the data from a server, for example.

**Services** allow us to get data from some provider, in this case, the Redbooth's OAuth API and the Redbooth's data API. **Models** represent the data handled by the app, as one would expect. Finally, we have some helpers, extensions and such that have no specific place in this diagram.

## Things to improve

* Error messages, I have tried to handle the different error situations the app may encounter using an enum, but I have the feeling it is not consistent and some of the error cases are duplicated.
* The app, given the current architecture, should be able to work without choosing a task. So this could be fixed.
* Right now, the focus, short break and long break durations are hardcoded in the app, a settings screen could be useful in order to let the user choose the values that make sense to her.
* The app is prepared for localization, but it is only available in English and without any generated strings file.
* I am not a designer and you can see it in the app :$