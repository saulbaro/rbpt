//
//  TokenTest.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 24/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import XCTest
import ObjectMapper
@testable import RBPT

class TokenTest: XCTestCase {
    
    private let accessToken = "1286198b9c618e86fcc7185338jddweet3wdasd342cf26a2bf84d5f7239b12343g"
    private let tokenType = "bearer"
    private let expiresIn = Double(7200)
    private let refreshToken = "c10e7790d130aasd3325e99ea3d123qiwfhfhgc3af93b2ce03506a63c7009992frt"
    private let scope = "all"
    private lazy var json: String = {
        return "{\"access_token\":\"\(self.accessToken)\",\"token_type\":\"\(self.tokenType)\",\"expires_in\":\(self.expiresIn),\"refresh_token\":\"\(self.refreshToken)\",\"scope\":\"\(self.scope)\"}"
        
    }()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testCreation() {
        let token = Mapper<Token>().map(json)!
        verifyTokenIntegrity(token)
        XCTAssert(!token.hasExpired(), "Token already expired after initialization")
    }
    
    func testKeychainConvertible() {
        let token = Mapper<Token>().map(json)!
        let tokenString = token.toString()!
        let tokenFromString = Token(tokenString)!
        verifyTokenIntegrity(tokenFromString, referenceToken: token)
        XCTAssert(tokenFromString.creationDate != token.creationDate, "Creation dates must be equal")
        XCTAssert(!token.hasExpired(), "Token already expired after initialization")
    }
    
    private func verifyTokenIntegrity(token: Token, referenceToken: Token? = nil) {
        XCTAssert(token.accessToken == referenceToken?.accessToken ?? accessToken, "Bad access token after initialization")
        XCTAssert(token.tokenType == referenceToken?.tokenType ?? tokenType, "Bad token type after initialization")
        XCTAssert(token.expiresIn == referenceToken?.expiresIn ?? expiresIn, "Bad expiration time after initialization")
        XCTAssert(token.refreshToken == referenceToken?.refreshToken ?? refreshToken, "Bad refresh token after initialization")
        XCTAssert(token.scope == referenceToken?.scope ?? scope, "Bad scope after initialization")
        XCTAssert(token.creationDate != nil, "Bad date after initialization")
    }
    
}