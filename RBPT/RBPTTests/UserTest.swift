//
//  UserTest.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 24/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import XCTest
import ObjectMapper
@testable import RBPT

class UserTest: XCTestCase {
    
    private let id = 1109852
    private lazy var json: String = {
        return "{\"id\":\(self.id)}"
    }()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testCreation() {
        let user: User = Mapper<User>().map(json)!
        XCTAssert(user.identifier == id, "Bad user identifier after initialization")
    }
    
    func testKeychainConvertible() {
        let user: User = Mapper<User>().map(json)!
        let userString = user.toString()!
        let userFromString = User(userString)!
        XCTAssert(userFromString.identifier == user.identifier, "Bad user identifier after keychain conversion")
    }
    
}

