//
//  UserTest.swift
//  RBPTTests
//
//  Created by Saül Baró Ruiz on 24/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import XCTest
@testable import RBPT

class PomodoroManagerTest: XCTestCase {
    
    private var configuration: PomodoroConfiguration!
    
    override func setUp() {
        super.setUp()
        let configuration = PomodoroConfiguration(
            focusDuration: 5,
            shorBreakDuration: 2,
            longBreakDuration: 3,
            numberOfFocusSessionsForALongBreak: 2)
        self.configuration = configuration
    }
    
    override func tearDown() {
        configuration = nil
        super.tearDown()
    }
    
    func testCreation() {
        let manager = PomodoroManager(configuration: configuration)
        XCTAssert(manager.currentSession.status == .NotStarted, "Session shouldn't be started after creation")
        XCTAssert(manager.currentSession.kind == .Focus, "First session must be a Focus one")
        XCTAssert(manager.currentSession.duration == configuration?.focusDuration, "Wrong focus session duration")
        XCTAssert(manager.currentSession.remainingTime == configuration?.focusDuration, "Remaining time should be equal to session duration")
    }
    
    func testPlaying() {
        let manager = PomodoroManager(configuration: configuration)
        manager.play()
        let expectation = expectationWithDescription("Focus session is playing")
        let time = configuration.focusDuration - 2
        runAfterDelay(time) { [weak self] _ in
            XCTAssert(manager.currentSession.status == .Playing, "Session must be playing")
            XCTAssert(manager.currentSession.kind == .Focus, "Session must a focus one")
            XCTAssert(manager.currentSession.duration == self?.configuration.focusDuration, "Wrong focus session duration")
            let expectedTime = (self?.configuration.focusDuration)! - time
            XCTAssert(manager.currentSession.remainingTime == expectedTime, "Wrong remaining time")
            expectation.fulfill()
        }
        waitForExpectationsWithTimeout(10, handler: nil)
    }
    
    func testPlayed() {
        let manager = PomodoroManager(configuration: configuration)
        manager.play()
        let expectation = expectationWithDescription("Focus session has finished")
        runAfterDelay(configuration.focusDuration) { [weak self] _ in
            XCTAssert(manager.currentSession.status == .NotStarted, "Next session shouldn't have started yet")
            XCTAssert(manager.currentSession.kind == .ShortBreak, "Second session must be a Focus one")
            XCTAssert(manager.currentSession.duration == self?.configuration.shorBreakDuration, "Wrong focus session duration")
            XCTAssert(manager.currentSession.remainingTime == self?.configuration.shorBreakDuration, "Remaining time should be equal to session duration")
            expectation.fulfill()
        }
        waitForExpectationsWithTimeout(10, handler: nil)
    }
    
    func testPause() {
        let manager = PomodoroManager(configuration: configuration)
        manager.play()
        let expectation = expectationWithDescription("Focus session has paused")
        let time = configuration.focusDuration - 2
        runAfterDelay(time) { [weak self] _ in
            manager.pause()
            XCTAssert(manager.currentSession.status == .Paused, "Session should be paused")
            XCTAssert(manager.currentSession.kind == .Focus, "First session must be a short break")
            XCTAssert(manager.currentSession.duration == self?.configuration.focusDuration, "Wrong focus session duration")
            let expectedTime = (self?.configuration.focusDuration)! - time
            XCTAssert(manager.currentSession.remainingTime == expectedTime, "Remaining time should be equal to session duration")
            expectation.fulfill()
        }
        waitForExpectationsWithTimeout(10, handler: nil)
    }
    
    func testReset() {
        let manager = PomodoroManager(configuration: configuration)
        let initialSession = manager.currentSession
        manager.play()
        let expectation = expectationWithDescription("Focus session has reseted")
        let time = configuration.focusDuration - 2
        runAfterDelay(time) { _ in
            manager.pause()
            manager.reset()
            XCTAssert(manager.currentSession.status == initialSession.status, "Status should be the same as the initial")
            XCTAssert(manager.currentSession.kind == initialSession.kind, "Kind should be the same as the initial")
            XCTAssert(manager.currentSession.duration == initialSession.duration, "Duration should be the same as the initial")
            XCTAssert(manager.currentSession.remainingTime == initialSession.remainingTime, "Remaining time should be the same as the initial")
            expectation.fulfill()
        }
        waitForExpectationsWithTimeout(10, handler: nil)
    }
    
    func testCompletion() {
        let manager = PomodoroManager(configuration: configuration)
        manager.play()
        let expectation = expectationWithDescription("Focus session has completed")
        let time = configuration.focusDuration - 2
        runAfterDelay(time) { [weak self] _ in
            manager.pause()
            manager.complete()
            XCTAssert(manager.currentSession.status == .NotStarted, "Next session shouldn't have started yet")
            XCTAssert(manager.currentSession.kind == .ShortBreak, "Second session must be a short break")
            XCTAssert(manager.currentSession.duration == self?.configuration.shorBreakDuration, "Wrong focus session duration")
            XCTAssert(manager.currentSession.remainingTime == self?.configuration.shorBreakDuration, "Remaining time should be equal to session duration")
            expectation.fulfill()
        }
        waitForExpectationsWithTimeout(10, handler: nil)
    }
    
    func testFullPomodoro() {
        let manager = PomodoroManager(configuration: configuration)
        manager.play()
        let expectation = expectationWithDescription("Full pomodoro has completed")
        runAfterDelay(configuration.focusDuration + 1) { [weak self] _ in
            XCTAssert(manager.currentSession.status == .NotStarted, "Next session shouldn't have started yet")
            XCTAssert(manager.currentSession.kind == .ShortBreak, "Second session must be a short break")
            XCTAssert(manager.currentSession.duration == self?.configuration.shorBreakDuration, "Wrong focus session duration")
            XCTAssert(manager.currentSession.remainingTime == self?.configuration.shorBreakDuration, "Remaining time should be equal to session duration")
            manager.play()
            self?.runAfterDelay((self?.configuration.shorBreakDuration)! + 1) { [weak self] _ in
                XCTAssert(manager.currentSession.status == .NotStarted, "Next session shouldn't have started yet")
                XCTAssert(manager.currentSession.kind == .Focus, "Third session must be a short break")
                XCTAssert(manager.currentSession.duration == self?.configuration.focusDuration, "Wrong focus session duration")
                XCTAssert(manager.currentSession.remainingTime == self?.configuration.focusDuration, "Remaining time should be equal to session duration")
                manager.play()
                self?.runAfterDelay((self?.configuration.focusDuration)! + 1) { [weak self] _ in
                    XCTAssert(manager.currentSession.status == .NotStarted, "Next session shouldn't have started yet")
                    XCTAssert(manager.currentSession.kind == .LongBreak, "Third session must be a short break")
                    XCTAssert(manager.currentSession.duration == self?.configuration.longBreakDuration, "Wrong focus session duration")
                    XCTAssert(manager.currentSession.remainingTime == self?.configuration.longBreakDuration, "Remaining time should be equal to session duration")
                    expectation.fulfill()
                }
            }
        }
        waitForExpectationsWithTimeout(20, handler: nil)
    }
    
    private func runAfterDelay(delay: NSTimeInterval, block: dispatch_block_t) {
        // Added 0.1 seconds of buffer in order to correct the possible delay of NSTimer
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64((delay + 0.1) * Double(NSEC_PER_SEC)))
        dispatch_after(time, dispatch_get_main_queue(), block)
    }
    
}
