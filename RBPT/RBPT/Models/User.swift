//
//  User.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 21/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation
import ObjectMapper

struct User: Mappable, KeychainStorableType {
    
    // MARK: Properties
    private(set) var identifier: Int!
    
    // MARK: - Protocols
    // MARK: Mappable
    init?(_ map: Map) {
        mapping(map)
    }
    
    mutating func mapping(map: Map) {
        identifier <- map["id"]
    }
    
}
