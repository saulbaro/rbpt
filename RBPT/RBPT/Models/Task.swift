//
//  Task.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 19/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation
import ObjectMapper

class Task: Mappable {
    
    enum Status: String {
        case New = "new"
        case Open = "open"
        case Hold = "hold"
        case Resolved = "resolved"
    }
    
    static let targetType = "task"
    
    // MARK: Properties
    private(set) var identifier: Int!
    private(set) var name: String!
    private(set) var isUrgent: Bool!
    private(set) var dueOn: String?
    private(set) var status: Status!
    
    // MARK: - Protocols
    // MARK: Mappable
    required init?(_ map: Map) {
        mapping(map)
    }
    
    func mapping(map: Map) {
        identifier  <- map["id"]
        name        <- map["name"]
        isUrgent    <- map["urgent"]
        dueOn       <- map["due_on"]
        status      <- map["status"]
    }
    
}
