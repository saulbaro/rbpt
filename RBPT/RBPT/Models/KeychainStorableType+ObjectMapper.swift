//
//  KeychainStorableType+ObjectMapper.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 21/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 *  Extension on KeychainStorableType to store Mappable 
 *  objects on the keychain.
 */
extension KeychainStorableType where Self: Mappable {
    
    typealias T = Self
    
    init?(_ string: String) {
        guard let jsonDictionary = Mapper<T>.parseJSONDictionary(string) else {
            return nil
        }
        let map = Map(mappingType: .FromJSON, JSONDictionary: jsonDictionary)
        self.init(map)
    }
    
    func toString() -> String? {
        return Mapper().toJSONString(self)
    }
    
}
