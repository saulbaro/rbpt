//
//  Token.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 18/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation
import ObjectMapper

struct Token: Mappable, KeychainStorableType {
    
    // MARK: Properties
    private(set) var accessToken: String!
    private(set) var tokenType: String!
    private(set) var expiresIn: Double!
    private(set) var refreshToken: String!
    private(set) var scope: String!
    private(set) var creationDate: NSDate!
    
    // MARK: Methods
    func hasExpired() -> Bool {
        let timeOffset = 100.0 // Adding some buffer for slow connections
        let expirationDate = NSDate(timeInterval: expiresIn - timeOffset, sinceDate: creationDate)
        return expirationDate.compare(NSDate()) == .OrderedAscending
    }
    
    // MARK: - Protocols
    // MARK: Mappable
    init?(_ map: Map) {
        mapping(map)
        if creationDate == nil {
            creationDate = NSDate()
        }
    }
    
    mutating func mapping(map: Map) {
        accessToken     <- map["access_token"]
        tokenType       <- map["token_type"]
        expiresIn       <- map["expires_in"]
        refreshToken    <- map["refresh_token"]
        scope           <- map["scope"]
        creationDate    <- (map["creation_date"], DateTransform())
    }
    
}
