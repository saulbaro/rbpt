//
//  Error.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 19/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation

/**
 *  An enum to map all the different errors that happen in the app.
 *  CAN BE improved
 */
enum Error: ErrorType {
    
    case Unauthorized
    case AuthorizationCodeIsEmpty
    case TokenIsNotValid
    case BadStatusCode(statusCode: Int)
    case APIError(message: String)
    case GenericError
    case InvalidURL
    case InvalidURLParameters
    case ParseError
    
}

// MARK: - Protocols
// MARK: CustomStringConvertible
extension Error: CustomStringConvertible {
    
    var description: String {
        switch self {
        case .AuthorizationCodeIsEmpty, .TokenIsNotValid, .InvalidURL, .InvalidURLParameters:
            return NSLocalizedString("Authorization didn't succeed. Please try again.", comment: "")
        case .BadStatusCode, .APIError:
            return NSLocalizedString("Something went wrong with the server. Please try again.", comment: "")
        case .ParseError:
            return NSLocalizedString("Server response couldn't be parsed. Please try again.", comment: "")
        case .GenericError:
            return NSLocalizedString("Something went wrong. Please try again.", comment: "")
        default:
            return self.description
        }
    }
    
}
