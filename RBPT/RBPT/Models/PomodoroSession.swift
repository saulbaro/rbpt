//
//  PomodoroSession.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 23/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation

struct PomodoroSession {
    
    enum Kind {
        case Focus
        case ShortBreak
        case LongBreak
    }
    
    enum Status {
        case NotStarted
        case Playing
        case Paused
        case Completed
    }
    
    // MARK: Properties
    private(set) var kind: Kind
    private(set) var duration: NSTimeInterval
    var remainingTime: NSTimeInterval
    var status: Status
    
    init(kind: Kind, duration: NSTimeInterval) {
        self.kind = kind
        self.duration = duration
        self.remainingTime = duration
        self.status = .NotStarted
    }
    
}
