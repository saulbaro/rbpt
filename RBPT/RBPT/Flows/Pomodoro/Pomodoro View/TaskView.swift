//
//  TaskView.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 24/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import UIKit
import Cartography

class TaskView: UIView {
    
    struct ViewData {
        let currentTaskButtonTitle: String?
    }
    
    var viewData: ViewData? {
        didSet {
            guard let viewData = viewData else { return }
            let currentTaskButtonTitle = viewData.currentTaskButtonTitle ?? NSLocalizedString("No task selected", comment: "")
            currentTaskButton.setTitle(currentTaskButtonTitle, forState: .Normal)
        }
    }
    
    private let currentTaskLabel: UILabel = {
        let currentTaskLabel = UILabel()
        currentTaskLabel.text = NSLocalizedString("Current task", comment: "")
        currentTaskLabel.textAlignment = .Center
        currentTaskLabel.textColor = AppTheme.Color.content
        currentTaskLabel.font = AppTheme.Font.content
        return currentTaskLabel
    }()
    
    let logoutButton: UIButton = {
        let logoutButton = UIButton(type: .System)
        logoutButton.setImage(UIImage(imageLiteral: "Logout"), forState: .Normal)
        return logoutButton
    }()
    
    let currentTaskButton: UIButton = {
        let currentTaskButton = UIButton(type: .System)
        currentTaskButton.setImage(UIImage(named: "Box"), forState: .Normal)
        currentTaskButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        currentTaskButton.titleLabel?.font = AppTheme.Font.button
        return currentTaskButton
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init() {
        super.init(frame: CGRect.zero)
        backgroundColor = UIColor.clearColor()
        setupViews()
        setupLayout()
    }
    
    private func setupViews() {
        addSubview(currentTaskLabel)
        addSubview(logoutButton)
        addSubview(currentTaskButton)
    }
    
    private func setupLayout() {
        constrain(self, logoutButton) { superview, logoutButton in
            logoutButton.leading == superview.leading
        }
        constrain(currentTaskLabel, logoutButton) { currentTaskLabel, logoutButton in
            logoutButton.centerY == currentTaskLabel.centerY
        }
        constrain(self, currentTaskLabel) { superview, currentTaskLabel in
            currentTaskLabel.top == superview.top + 28
            currentTaskLabel.centerX == superview.centerX
        }
        constrain(self, currentTaskButton) { superview, currentTaskButton in
            currentTaskButton.leading == superview.leading
            currentTaskButton.trailing == superview.trailing
        }
        constrain(currentTaskLabel, currentTaskButton) { currentTaskLabel, currentTaskButton in
            currentTaskButton.top == currentTaskLabel.bottom + 10
        }
    }
    
}
