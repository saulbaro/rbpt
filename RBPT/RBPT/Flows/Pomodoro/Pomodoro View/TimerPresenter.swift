//
//  TimerPresenter.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 23/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import UIKit

struct TimerPresenter {
    
    private(set) lazy var viewData: TimerView.ViewData? = {
        let viewData = TimerView.ViewData(
            progress: self.progress(),
            circleColor: self.color(),
            kind: self.kind(),
            time: self.formattedTime(),
            playButtonHidden: self.playButtonHidden(),
            pauseButtonHidden: self.pauseButtonHidden(),
            resetButtonHidden: self.resetButtonHidden(),
            completeButtonHidden: self.completeButtonHidden()
        )
        return viewData
    }()
    
    private var pomodoroSession: PomodoroSession?
    
    init(pomodoroSession: PomodoroSession?) {
        self.pomodoroSession = pomodoroSession
    }
    
    private func kind() -> String {
        guard let pomodoroSession = pomodoroSession else { return "" }
        let kind: String
        switch pomodoroSession.kind {
        case .Focus:
            kind = NSLocalizedString("Focus Session", comment: "")
        case .ShortBreak:
            kind = NSLocalizedString("Short Break", comment: "")
        case .LongBreak:
            kind = NSLocalizedString("Long Break", comment: "")
        }
        return kind
    }
    
    private func progress() -> CGFloat {
        guard let pomodoroSession = pomodoroSession else { return 1.0 }
        return CGFloat(pomodoroSession.remainingTime) / CGFloat(pomodoroSession.duration)
    }
    
    private func color() -> UIColor {
        guard let pomodoroSession = pomodoroSession else { return AppTheme.Color.circleColorFocus }
        let color: UIColor
        switch pomodoroSession.kind {
        case .Focus:
            color = AppTheme.Color.circleColorFocus
        case .ShortBreak:
            color = AppTheme.Color.circleColorBreak
        case .LongBreak:
            color = AppTheme.Color.circleColorLongBreak
        }
        return color
    }
    
    private func formattedTime() -> String {
        guard let pomodoroSession = pomodoroSession else { return "" }
        return pomodoroSession.remainingTime.minutesAndSecondsString()
    }
    
    private func playButtonHidden() -> Bool {
        guard let pomodoroSession = pomodoroSession else { return false }
        return pomodoroSession.status == .Playing
    }
    
    private func pauseButtonHidden() -> Bool {
        guard let pomodoroSession = pomodoroSession else { return false }
        return pomodoroSession.status != .Playing
    }
    
    private func resetButtonHidden() -> Bool {
        guard let pomodoroSession = pomodoroSession else { return false }
        return pomodoroSession.status != .Paused
    }
    
    private func completeButtonHidden() -> Bool {
        guard let pomodoroSession = pomodoroSession else { return false }
        return pomodoroSession.status != .Paused
    }
    
}
