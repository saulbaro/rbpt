//
//  PomodoroView.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 18/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import UIKit
import Cartography

/**
 *  The Pomodoro view, in this case, since the data is complex, it
 *  is setted using multiple views and ViewData structs that
 *  handle both views' values.
 */
class PomodoroView: UIView {
    
    let taskView: TaskView = {
        return TaskView()
    }()
    
    let timerView: TimerView = {
        return TimerView()
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init() {
        super.init(frame: CGRect.zero)
        backgroundColor = AppTheme.Color.background
        setupViews()
        setupLayout()
    }
    
    private func setupViews() {
        addSubview(taskView)
        addSubview(timerView)
    }
    
    private func setupLayout() {
        constrain(self, taskView) { superview, taskView in
            taskView.top == superview.top
            taskView.leading == superview.leadingMargin
            taskView.trailing == superview.trailingMargin
        }
        constrain(self, timerView) { superview, timerView in
            timerView.bottom == superview.bottom
            timerView.leading == superview.leadingMargin
            timerView.trailing == superview.trailingMargin
        }
        constrain(taskView, timerView) { taskView, timerView in
            timerView.top == taskView.bottom
        }
        constrain(self, taskView) { superview, taskView in
            taskView.bottom == superview.centerY - timerView.circularProgressHeight / 2
        }
        
    }
    
}
