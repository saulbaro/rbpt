//
//  TaskPresenter.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 24/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import UIKit

struct TaskPresenter {
    
    private(set) lazy var viewData: TaskView.ViewData? = {
        let viewData = TaskView.ViewData(
            currentTaskButtonTitle: self.task?.name ?? NSLocalizedString("No task selected", comment: "")
        )
        return viewData
    }()
    
    private weak var task: Task?
    
    init(task: Task?) {
        self.task = task
    }
    
}