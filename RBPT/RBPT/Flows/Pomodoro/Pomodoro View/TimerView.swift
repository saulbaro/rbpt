//
//  TimerView.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 24/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import UIKit
import Cartography
import DACircularProgress

class TimerView: UIView {
    
    let circularProgressHeight: CGFloat = 250
    
    struct ViewData {
        let progress: CGFloat
        let circleColor: UIColor
        let kind: String
        let time: String
        let playButtonHidden: Bool
        let pauseButtonHidden: Bool
        let resetButtonHidden: Bool
        let completeButtonHidden: Bool
    }
    
    var viewData: ViewData? {
        didSet {
            guard let viewData = viewData else { return }
            circularProgress.progress = viewData.progress
            circularProgress.progressTintColor = viewData.circleColor
            kindLabel.text = viewData.kind
            timeLabel.text = viewData.time
            playButton.hidden = viewData.playButtonHidden
            pauseButton.hidden = viewData.pauseButtonHidden
            resetButton.hidden = viewData.resetButtonHidden
            completeButton.hidden = viewData.completeButtonHidden
        }
    }
    
    private let circularProgress: DACircularProgressView = {
        let circularProgress = DACircularProgressView()
        circularProgress.clockwiseProgress = 0
        circularProgress.roundedCorners = 0
        circularProgress.thicknessRatio = 0.02
        return circularProgress
    }()
    
    private let kindLabel: UILabel = {
        let kindLabel = UILabel()
        kindLabel.font = AppTheme.Font.content
        kindLabel.textAlignment = .Center
        kindLabel.textColor = AppTheme.Color.content
        return kindLabel
    }()
    
    private let timeLabel: UILabel = {
        let timeLabel = UILabel()
        timeLabel.font = AppTheme.Font.time
        timeLabel.textAlignment = .Center
        timeLabel.textColor = AppTheme.Color.content
        return timeLabel
    }()
    
    
    let playButton: UIButton = {
        let playButton = UIButton(type: .System)
        playButton.setImage(UIImage(imageLiteral: "Play"), forState: .Normal)
        return playButton
    }()
    
    let pauseButton: UIButton = {
        let pauseButton = UIButton(type: .System)
        pauseButton.setImage(UIImage(imageLiteral: "Pause"), forState: .Normal)
        return pauseButton
    }()
    
    let resetButton: UIButton = {
        let resetButton = UIButton(type: .System)
        resetButton.setTitle(NSLocalizedString("Reset", comment: ""), forState: .Normal)
        resetButton.titleLabel?.font = AppTheme.Font.button
        return resetButton
    }()
    
    let completeButton: UIButton = {
        let completeButton = UIButton(type: .System)
        completeButton.setTitle(NSLocalizedString("Complete", comment: ""), forState: .Normal)
        completeButton.titleLabel?.font = AppTheme.Font.button
        return completeButton
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init() {
        super.init(frame: CGRect.zero)
        backgroundColor = AppTheme.Color.background
        setupViews()
        setupLayout()
    }
    
    private func setupViews() {
        addSubview(circularProgress)
        addSubview(kindLabel)
        addSubview(timeLabel)
        addSubview(playButton)
        addSubview(pauseButton)
        addSubview(resetButton)
        addSubview(completeButton)
    }
    
    private func setupLayout() {
        constrain(self, circularProgress) { superview, circularProgress in
            circularProgress.top == superview.top
            circularProgress.width == circularProgressHeight
            circularProgress.height == circularProgress.width
            circularProgress.centerX == superview.centerX
        }
        constrain(circularProgress, kindLabel, timeLabel, playButton) { circularProgress, kindLabel, timeLabel, playButton in
            timeLabel.center == circularProgress.center
            kindLabel.centerX == circularProgress.centerX
            kindLabel.bottom == timeLabel.top - 20
            playButton.centerX == circularProgress.centerX
            playButton.top == timeLabel.bottom + 20
        }
        constrain(playButton, pauseButton) { playButton, pauseButton in
            pauseButton.center == playButton.center
        }
        constrain(self, resetButton) { superview, resetButton in
            resetButton.bottom == superview.bottom - 10
            resetButton.leading == superview.leading
        }
        constrain(self, completeButton) { superview, completeButton in
            completeButton.bottom == superview.bottom - 10
            completeButton.trailing == superview.trailing
        }
    }
    
}
