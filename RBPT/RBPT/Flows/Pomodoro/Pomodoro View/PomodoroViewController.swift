//
//  PomodoroViewController.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 18/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import UIKit

protocol PomodoroViewControllerDelegate: class {
    func pomodoroViewControllerDidTapLogoutButton(controller: PomodoroViewController)
    func pomodoroViewControllerDidTapTaskButton(controller: PomodoroViewController)
    func pomodoroViewControllerDidTapPlayButton(controller: PomodoroViewController)
    func pomodoroViewControllerDidTapPauseButton(controller: PomodoroViewController)
    func pomodoroViewControllerDidTapResetButton(controller: PomodoroViewController)
    func pomodoroViewControllerDidTapCompleteButton(controller: PomodoroViewController)
}

class PomodoroViewController: UIViewController {
    
    var taskPresenter: TaskPresenter? {
        didSet {
            pomodoroView.taskView.viewData = taskPresenter?.viewData
        }
    }
    
    var timerPresenter: TimerPresenter? {
        didSet {
            pomodoroView.timerView.viewData = timerPresenter?.viewData
        }
    }
    
    weak var delegate: PomodoroViewControllerDelegate?
    
    private(set) lazy var pomodoroView: PomodoroView = {
        let pomodoroView = PomodoroView()        
        pomodoroView.taskView.logoutButton.addTarget(self, action: "logoutButtonTapped", forControlEvents: .TouchUpInside)
        pomodoroView.taskView.currentTaskButton.addTarget(self, action: "taskButtonTapped", forControlEvents: .TouchUpInside)
        pomodoroView.timerView.playButton.addTarget(self, action: "playButtonTapped", forControlEvents: .TouchUpInside)
        pomodoroView.timerView.pauseButton.addTarget(self, action: "pauseButtonTapped", forControlEvents: .TouchUpInside)
        pomodoroView.timerView.resetButton.addTarget(self, action: "resetButtonTapped", forControlEvents: .TouchUpInside)
        pomodoroView.timerView.completeButton.addTarget(self, action: "completeButtonTapped", forControlEvents: .TouchUpInside)
        return pomodoroView
    }()
    
    override func loadView() {
        view = pomodoroView
    }
    
    // MARK: - Private API
    // MARK: Button Actions
    func logoutButtonTapped() {
        delegate?.pomodoroViewControllerDidTapLogoutButton(self)
    }
    
    func taskButtonTapped() {
        delegate?.pomodoroViewControllerDidTapTaskButton(self)
    }
    
    func playButtonTapped() {
        delegate?.pomodoroViewControllerDidTapPlayButton(self)
    }
    
    func pauseButtonTapped() {
        delegate?.pomodoroViewControllerDidTapPauseButton(self)
    }
    
    func resetButtonTapped() {
        delegate?.pomodoroViewControllerDidTapResetButton(self)
    }
    
    func completeButtonTapped() {
        delegate?.pomodoroViewControllerDidTapCompleteButton(self)
    }
    
}
