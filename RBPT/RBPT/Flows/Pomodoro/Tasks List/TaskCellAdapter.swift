//
//  TaskCellAdapter.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 23/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import UIKit

class TaskCellAdapter: CellAdapterType {
    
    let CellIdentifier = "CellIdentifier"
    
    func cellForTableView(tableView: UITableView, item: AnyObject?, atIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier, forIndexPath: indexPath)
        guard let task = item as? Task else {
            return cell
        }
        cell.textLabel?.text = task.name
        cell.detailTextLabel?.text = task.isUrgent == true ? NSLocalizedString("Urgent", comment: "") : task.dueOn
        return cell
    }
    
    func registerClassOrNibForTableView(tableView: UITableView) {
        tableView.registerClass(TaskTableViewCell.self, forCellReuseIdentifier: CellIdentifier)
    }
    
}
