//
//  TasksViewController.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 23/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import UIKit

protocol TasksViewControllerDelegate: class {
    func tasksViewControllerDidTapCloseButton(controller: TasksViewController)
    func tasksViewController(controller: TasksViewController, didSelectTask task: Task)
}

class TasksViewController: UITableViewController {
    
    private lazy var closeButton: UIBarButtonItem = {
        let closeButton = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: "closeButtonTapped")
        closeButton.style = .Plain
        return closeButton
    }()
    
    weak var delegate: TasksViewControllerDelegate?
    private var datasource: SectionedDataSource?
    
    init(tasks: [(String, [Task])]) {
        super.init(style: .Plain)
        title = NSLocalizedString("Choose a task", comment: "")
        navigationItem.leftBarButtonItem = closeButton
        
        let datasource = SectionedDataSource(tableView: tableView, cellAdapter: TaskCellAdapter())
        datasource.items = tasks.map { ($0.0, $0.1 as [AnyObject]) }
        self.datasource = datasource
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Private API
    // MARK: Button Actions
    func closeButtonTapped() {
        delegate?.tasksViewControllerDidTapCloseButton(self)
    }
    
    
    // MARK: - Protocols
    // MARK: UITableViewDelegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard let task = datasource?.itemAtIndexPath(indexPath) as? Task else {
            fatalError("The datasource shouldn't be nil and should only return Task objects")
        }
        delegate?.tasksViewController(self, didSelectTask: task)
    }
    
}
