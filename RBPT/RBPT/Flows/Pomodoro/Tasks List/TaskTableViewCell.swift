//
//  TaskTableViewCell.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 23/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .Subtitle, reuseIdentifier: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
