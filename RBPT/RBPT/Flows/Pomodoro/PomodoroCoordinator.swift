//
//  PomodoroCoordinator.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 17/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import UIKit


protocol PomodoroCoordinatorDelegate: class {
    func coordinatorDidCloseSession(coordinator: PomodoroCoordinator)
}

/**
 *  Responsible for the Pomodoro flow
 */
class PomodoroCoordinator {
    
    weak var delegate: PomodoroCoordinatorDelegate?
    
    private weak var rootController: RootViewController?
    private weak var sessionManager: SessionManagerType?
    private let pomodoroManager: PomodoroManagerType
    private let taskManager: TaskManagerType
    
    private weak var pomodoroController: PomodoroViewController?
    private var currentTask: Task? {
        didSet {
            pomodoroController?.taskPresenter = TaskPresenter(task: currentTask)
        }
    }
    
    init(
        rootController: RootViewController,
        sessionManager: SessionManagerType,
        pomodoroManager: PomodoroManagerType = PomodoroManager()
        ) {
            self.rootController = rootController
            self.sessionManager = sessionManager
            self.pomodoroManager = pomodoroManager
            self.taskManager = RedboothAPIManager(sessionManager: sessionManager)
            sessionManager.delegate = self
            pomodoroManager.delegate = self
    }
    
    // MARK: Public API
    func start() {
        guard let rootController = rootController else {
            fatalError("rootController can't be nil")
        }
        let pomodoroViewController = PomodoroViewController()
        pomodoroViewController.taskPresenter = TaskPresenter(task: currentTask)
        pomodoroViewController.timerPresenter = TimerPresenter(pomodoroSession: pomodoroManager.currentSession)
        pomodoroViewController.delegate = self
        rootController.contentViewController = pomodoroViewController
        pomodoroController = pomodoroViewController
    }
    
    // MARK: Private API
    // MARK: UI Helpers
    private func showTasksList() {
        rootController?.showLoadingViewWithMessage(NSLocalizedString("Fetching tasks", comment: ""))
        taskManager.fetchOpenTasksAssignedToUserGroupedByFuzzyDate { [weak self] result in
            self?.rootController?.hideLoadingView()
            switch result {
            case .Success(let tasks):
                self?.presentTasksViewControllerWithTasks(tasks)
            case .Failure(let error):
                self?.rootController?.showErrorWithMessage(error.description)
            }
        }
    }
    
    private func presentTasksViewControllerWithTasks(groupedTasks: [(String, [Task])]) {
        let tasksVC = TasksViewController(tasks: groupedTasks)
        tasksVC.delegate = self
        let navigationVC = UINavigationController(rootViewController: tasksVC)
        navigationVC.navigationBar.barStyle = .Black
        rootController?.presentViewController(navigationVC, animated: true, completion: nil)
    }
    
    private func showMenu(title: String, actions: [UIAlertAction]) {
        let menu = UIAlertController(title: title, message: nil, preferredStyle: .ActionSheet)
        actions.forEach { action in menu.addAction(action) }
        rootController?.presentViewController(menu, animated: true, completion: nil)
    }
    
    private func showCurrentTaskOptions() {
        let actions = [
            UIAlertAction(title: NSLocalizedString("Change Task", comment: ""), style: .Default) { [weak self] _ in
                self?.showTasksList()
            },
            UIAlertAction(title: NSLocalizedString("Close Task", comment: ""), style: .Destructive) { [weak self] _ in
                self?.closeCurrentTask()
            },
            UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .Cancel, handler: nil)
        ]
        let menuTitle = NSLocalizedString("What do you want to do?", comment: "")
        showMenu(menuTitle, actions: actions)
    }
    
    private func showLogoutOptions() {
        let actions = [
            UIAlertAction(title: NSLocalizedString("Discard progress", comment: ""), style: .Destructive) { [weak self] _ in
                self?.sessionManager?.closeSession()
            },
            UIAlertAction(title: NSLocalizedString("Close Task", comment: ""), style: .Destructive) { [weak self] _ in
                self?.closeCurrentTask()
                self?.sessionManager?.closeSession()
            },
            UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .Cancel, handler: nil)
        ]
        let menuTitle = NSLocalizedString("What do you want to do?", comment: "")
        showMenu(menuTitle, actions: actions)
    }
    
    // MARK: Data helpers
    private func addTimeToCurrentTask(time: NSTimeInterval) {
        guard let currentTask = currentTask else {
            return
        }
        rootController?.showLoadingViewWithMessage(NSLocalizedString("Finishing focus session", comment: ""))
        taskManager.addTime(time, toTask: currentTask) { [weak self] result in
            self?.rootController?.hideLoadingView()
            if case .Failure(let error) = result {
                self?.rootController?.showErrorWithMessage(error.description)
            }
        }
    }
    
    private func closeCurrentTask() {
        guard let currentTask = currentTask else {
            return
        }
        rootController?.showLoadingViewWithMessage(NSLocalizedString("Closing Task", comment: ""))
        taskManager.closeTask(currentTask) { [weak self] result in
            self?.rootController?.hideLoadingView()
            switch result {
            case .Success:
                self?.currentTask = nil
            case .Failure(let error):
                self?.rootController?.showErrorWithMessage(error.description)
            }
        }
    }
    
}

// MARK: - Protocols
// MARK: OAuthManagerDelegate
extension PomodoroCoordinator: SessionManagerDelegate {
    
    func managerGotKickedOut(manager: SessionManagerType) {
        pomodoroManager.reset()
        sessionManager?.delegate = nil
        taskManager.clearManager()
        delegate?.coordinatorDidCloseSession(self)
    }
    
}

// MARK: PomodoroManagerTypeDelegate
extension PomodoroCoordinator: PomodoroManagerTypeDelegate {
    
    func manager(manager: PomodoroManagerType, sessionDidUpdate session: PomodoroSession) {
        pomodoroController?.timerPresenter = TimerPresenter(pomodoroSession: session)
        guard session.kind == .Focus && session.status == .Completed else {
            return
        }
        let timeSpent = session.duration - session.remainingTime
        addTimeToCurrentTask(timeSpent)
    }
    
}

// MARK: PomodoroViewControllerDelegate
extension PomodoroCoordinator: PomodoroViewControllerDelegate {
    
    func pomodoroViewControllerDidTapLogoutButton(controller: PomodoroViewController) {
        guard pomodoroManager.currentSession.status == .NotStarted else {
            showLogoutOptions()
            return
        }
        sessionManager?.closeSession()
    }
    
    func pomodoroViewControllerDidTapTaskButton(controller: PomodoroViewController) {
        guard let _ = currentTask else {
            showTasksList()
            return
        }
        guard pomodoroManager.currentSession.status == .NotStarted else {
            rootController?.showMessage(
                NSLocalizedString("Please finish the current pomodoro before changing tasks.", comment: ""),
                withTitle: NSLocalizedString("Pomodoro is running...", comment: ""))
            return
        }
        showCurrentTaskOptions()
    }
    
    func pomodoroViewControllerDidTapPlayButton(controller: PomodoroViewController) {
        guard let _ = currentTask else {
            showTasksList()
            return
        }
        pomodoroManager.play()
    }
    
    func pomodoroViewControllerDidTapPauseButton(controller: PomodoroViewController) {
        pomodoroManager.pause()
    }
    
    func pomodoroViewControllerDidTapResetButton(controller: PomodoroViewController) {
        pomodoroManager.reset()
    }
    
    func pomodoroViewControllerDidTapCompleteButton(controller: PomodoroViewController) {
        pomodoroManager.complete()
    }
    
}

extension PomodoroCoordinator: TasksViewControllerDelegate {
    
    func tasksViewControllerDidTapCloseButton(controller: TasksViewController) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func tasksViewController(controller: TasksViewController, didSelectTask task: Task) {
        currentTask = task
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
