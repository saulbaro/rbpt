//
//  WelcomeView.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 17/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import UIKit
import Cartography

/**
 *  The Welcome view, its data (label and buttons text) could be
 *  set using a ViewData struct and a Presenter, but it seemed unnecessary
 *  for such a simple view
 */
class WelcomeView: UIView {
    
    private let titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.text = NSLocalizedString("Welcome", comment: "")
        titleLabel.textColor = AppTheme.Color.content
        titleLabel.font = AppTheme.Font.title
        return titleLabel
    }()
    
    private let contentLabel: UILabel = {
        let contentLabel = UILabel()
        contentLabel.text = NSLocalizedString("RBPT needs access to your Redbooth account in order to help you manage your tasks.", comment: "")
        contentLabel.numberOfLines = 0
        contentLabel.textAlignment = .Center
        contentLabel.textColor = AppTheme.Color.content
        contentLabel.font = AppTheme.Font.content
        return contentLabel
    }()
    
    let authorizeButton: UIButton = {
        let authorizeButton = UIButton(type: .Custom)
        authorizeButton.setTitle(NSLocalizedString("Authorize", comment: ""), forState: .Normal)
        authorizeButton.layer.cornerRadius = 5
        authorizeButton.titleLabel?.font = AppTheme.Font.button
        authorizeButton.setTitleColor(AppTheme.Color.buttonTitle, forState: .Normal)
        authorizeButton.setTitleColor(AppTheme.Color.buttonTitle.colorWithAlphaComponent(0.5), forState: .Highlighted)
        authorizeButton.backgroundColor = AppTheme.Color.buttonBackground
        return authorizeButton
    }()
    
    init() {
        super.init(frame: CGRect.zero)
        backgroundColor = AppTheme.Color.background
        setupViews()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        addSubview(titleLabel)
        addSubview(contentLabel)
        addSubview(authorizeButton)
    }
    
    private func setupLayout() {
        constrain(self, titleLabel) { superview, title in
            title.top == superview.top + 100
            title.centerX == superview.centerX
        }
        constrain(self, contentLabel) { superview, content in
            content.centerY == superview.centerY
            content.leading == superview.leading + 50
            content.trailing == superview.trailing - 50
        }
        constrain(self, authorizeButton) { superview, button in
            button.height == 50
            button.leading == superview.leading + 60
            button.trailing == superview.trailing - 60
            button.bottom == superview.bottom - 60
        }
    }
    
}
