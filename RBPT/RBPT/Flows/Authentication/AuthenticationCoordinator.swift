//
//  AuthenticationCoordinator.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 17/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import UIKit
import SafariServices

protocol AuthenticationCoordinatorDelegate {
    func coordinatorDidAuthenticate(coordinator: AuthenticationCoordinator)
}

/**
 *  Responsible for the Authentication flow
 */
class AuthenticationCoordinator: NSObject {
    
    var delegate: AuthenticationCoordinatorDelegate?
    
    private weak var rootController: RootViewController?
    private weak var sessionManager: SessionManagerType?
    
    init(rootController: RootViewController, sessionManager: SessionManagerType) {
        self.rootController = rootController
        self.sessionManager = sessionManager
        super.init()
        sessionManager.authorizationDelegate = self
    }
    
    // MARK: Public API
    func start() {
        guard let rootController = rootController else {
            fatalError("rootController can't be nil")
        }
        let welcomeViewController = WelcomeViewController()
        welcomeViewController.delegate = self
        rootController.contentViewController = welcomeViewController
    }
    
    // MARK: Private API
    private func showAuthorizationWebView() {
        guard let authorizationURL = sessionManager?.authorizationURL else { return }
        let authenticationViewController = SFSafariViewController(URL: authorizationURL)
        authenticationViewController.delegate = self
        rootController?.presentViewController(authenticationViewController, animated: true, completion: nil)
    }
    
    private func didReceiveAuthorizationCode(authorizationCode: String) {
        rootController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
}

// MARK: - Protocols
// MARK: OAuthManagerDelegate
extension AuthenticationCoordinator: SessionManagerAuthorizationDelegate {
    
    func managerDidAuthorize(manager: SessionManagerType) {
        rootController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func managerGotToken(manager: SessionManagerType) {
        delegate?.coordinatorDidAuthenticate(self)
    }
    
}

// MARK: WelcomeViewControllerDelegate
extension AuthenticationCoordinator: WelcomeViewControllerDelegate {
    
    func welcomeViewControllerDidTapAuthorizeButton(controller: WelcomeViewController) {
        showAuthorizationWebView()
    }
    
}

// MARK: SFSafariViewControllerDelegate
extension AuthenticationCoordinator: SFSafariViewControllerDelegate {
    
    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
