//
//  WelcomeViewController.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 17/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import UIKit

protocol WelcomeViewControllerDelegate: class {
    func welcomeViewControllerDidTapAuthorizeButton(controller: WelcomeViewController)
}

class WelcomeViewController: UIViewController {
    
    weak var delegate: WelcomeViewControllerDelegate?
    
    private(set) lazy var welcomeView: WelcomeView = {
        let welcomeView = WelcomeView()
        welcomeView.authorizeButton.addTarget(self, action: "authorizeButtonTapped", forControlEvents: .TouchUpInside)
        return welcomeView
    }()
    
    override func loadView() {
        view = welcomeView
    }
    
    /**
     *   This method can't be private in order to be seen by the Obj-C runtime
     */
    func authorizeButtonTapped() {
        delegate?.welcomeViewControllerDidTapAuthorizeButton(self)
    }
    
}
