//
//  KeychainWrapper.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 21/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation

/**
 *  Protocol an object has to subscribe to in order to be stored in the Keychain
 */
protocol KeychainStorableType {
    init?(_ String: String)
    func toString() -> String?
}

import KeychainAccess


/**
 *  Wrapper on top of the Keychain to store API components
 */
class KeychainWrapper {
    
    // Keychain wrapper
    private let keychain: Keychain
    
    init(serviceName: String) {
        self.keychain = Keychain(service: serviceName)
    }
    
    // MARK: Public API
    func fetchValueForKey(key: String) -> String? {
        return keychain[key]
    }
    
    func store(value: KeychainStorableType?, forKey key: String) {
        keychain[key] = value?.toString()
    }
    
    subscript(key: String) -> String? {
        get {
            return keychain[key]
        }
        set {
            keychain[key] = newValue
        }
    }
    
    func clear() {
        fatalError("Abstract method")
    }
    
}
