//
//  NSTimeInterval+String.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 23/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation

/**
 *  Extension on NSTimeInterval to convert it into a minutes:seconds string
 */
extension NSTimeInterval {
    
    func minutesAndSecondsString() -> String {
        let time = NSInteger(self)
        let seconds = time % 60
        let minutes = (time / 60) % 60
        return NSString(format: "%0.2d:%0.2d", minutes, seconds) as String
    }
    
}