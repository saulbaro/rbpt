//
//  SectionedDataSource.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 23/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import UIKit

protocol CellAdapterType {
    func cellForTableView(tableView: UITableView, item: AnyObject?, atIndexPath indexPath: NSIndexPath) -> UITableViewCell
    func registerClassOrNibForTableView(tableView: UITableView)
}

/** 
 *  A generic data source to handle sectioned data
 */
class SectionedDataSource: NSObject {
    
    private weak var tableView: UITableView?
    private let cellAdapter: CellAdapterType
    
    /**
     The items that the table view should display. Setting this property will cause de table view to reload.
     */
    var items: [(String, [AnyObject])]! {
        didSet {
            tableView?.reloadData()
        }
    }
    
    init(tableView: UITableView, cellAdapter: CellAdapterType) {
        self.tableView = tableView
        self.cellAdapter = cellAdapter
        super.init()
        tableView.dataSource = self
        cellAdapter.registerClassOrNibForTableView(tableView)
    }
    
    /**
     The item at the corresponding index path.
     */
    func itemAtIndexPath(indexPath: NSIndexPath) -> AnyObject? {
        return items[indexPath.section].1.count > indexPath.row ? items[indexPath.section].1[indexPath.row] : nil
    }
    
}

extension SectionedDataSource: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].1.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return cellAdapter.cellForTableView(tableView, item: itemAtIndexPath(indexPath), atIndexPath: indexPath)
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return items[section].0
    }
}
