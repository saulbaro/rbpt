//
//  RedboothAPI.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 18/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper

/**
 The Redbooth API endpoints needed by the app
 
 - GetUserInfo:      Used to get the logged user information
 - GetTasks:         Used to get task objects
 - UpdateTaskStatus: Used to update taks objects
 - CreateComment:    Used to create comment objects
 */
enum RedboothAPI {
    case GetUserInfo
    case GetTasks(assignedUserIdentifier: Int, status: String)
    case UpdateTaskStatus(taskIdentifier: Int, newStatus: String)
    case CreateComment(targetType: String, targetIdentifier: Int, minutes: Int)
}

extension RedboothAPI: TargetType {
    var baseURL: NSURL { return NSURL(string: "https://redbooth.com/api/3")! }
    
    var path: String {
        switch self {
        case .GetUserInfo:
            return "/me"
        case .GetTasks:
            return "/tasks"
        case .UpdateTaskStatus(let taskIdentifier, _):
            return "/tasks/\(taskIdentifier)"
        case .CreateComment:
            return "/comments"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .UpdateTaskStatus:
            return .PUT
        case .CreateComment:
            return .POST
        default:
            return .GET
        }
    }
    
    var parameters: [String: AnyObject]? {
        let parameters: [String: AnyObject]?
        switch self {
        case .GetUserInfo:
            parameters = nil
        case .GetTasks(let assignedUserIdentifier, let status):
            parameters = [
                "assigned_user_id" : assignedUserIdentifier,
                "status" : status
            ]
        case .UpdateTaskStatus(_, let newStatus):
            parameters = [
                "status" : newStatus
            ]
        case .CreateComment(let targetType, let targetIdentifier, let minutes):
            parameters = [
                "target_type" : targetType,
                "target_id" : targetIdentifier,
                "minutes" : minutes
            ]
        }
        return parameters
    }
    
    // TODO: Add sample data for testing
    var sampleData: NSData {
        return NSData()
    }
}
