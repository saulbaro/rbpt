//
//  RedboothOAuth.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 18/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation

/**
 OAuthService - Defines the basic endpoints an OAuth API must have.
 
 - GetToken:     Identifies the GetToken operation on an OAuth API and its parameters
 - RefreshToken: Identifies the RefreshToken operation on an OAuth API and its parameters
 */
enum OAuthService {
    case GetToken(clientId: String, clientSecret: String, code: String, redirectURI: String)
    case RefreshToken(clientId: String, clientSecret: String, refreshToken: String)
}
