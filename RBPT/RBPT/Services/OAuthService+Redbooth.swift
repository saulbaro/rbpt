//
//  OAuthService+Redbooth.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 19/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation
import Moya

/**
 *  We extend OAuthService to work with the Redbooth OAuth endpoint.
 *  If we wanted our app to work with another service, we could just replace
 *  the extension with another one.
 */
extension OAuthService: TargetType {
    var baseURL: NSURL { return NSURL(string: "https://redbooth.com/oauth2/")! }
    
    var path: String {
        switch self {
        case .GetToken:
            return "/token"
        case .RefreshToken:
            return "/token"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .GetToken, .RefreshToken:
            return .POST
        }
    }
    
    var parameters: [String: AnyObject]? {
        let parameters: [String: AnyObject]?
        switch self {
        case .GetToken(let clientId, let clientSecret, let code, let redirectURI):
            parameters = [
                "client_id" : clientId,
                "client_secret" : clientSecret,
                "code" : code,
                "grant_type" : "authorization_code",
                "redirect_uri" : redirectURI
            ]
        case .RefreshToken(let clientId, let clientSecret, let refreshToken):
            parameters = [
                "client_id" : clientId,
                "client_secret" : clientSecret,
                "refresh_token" : refreshToken,
                "grant_type" : "refresh_token"
            ]
        }
        return parameters
    }
    
    var sampleData: NSData {
        return NSData()
    }
    
}
