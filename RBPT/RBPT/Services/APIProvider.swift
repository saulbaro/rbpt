//
//  APIProvider.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 23/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation
import Moya
import Result

/**
 *  Extension on MoyaProvider to map and filter responses
 */
extension MoyaProvider {
    
    func performRequest<T>(endpoint: Target, withExpectedStatusCode expectedStatusCode: Int, mapper: (Response) -> T?, completion: (Result<T, Error>) -> ()) -> Cancellable {
        return self.request(endpoint) { result in
            let filteredResult = MoyaProvider.filterResult(result, withExpectedStatusCode: expectedStatusCode)
            switch filteredResult {
            case .Success(let response):
                guard let parsedObject = mapper(response) else {
                    completion(.Failure(.ParseError))
                    return
                }
                completion(.Success(parsedObject))
            case .Failure(let error):
                completion(.Failure(error))
            }
        }
    }
    
    static func filterResult(result: Result<Response, Moya.Error>, withExpectedStatusCode expectedStatusCode: Int) -> Result<Response, Error> {
        switch result {
        case .Success(let response) where response.statusCode == expectedStatusCode:
            return .Success(response)
        case .Success(let response) where response.statusCode == 401:
            return .Failure(.Unauthorized)
        case .Success(let response):
            return .Failure(.BadStatusCode(statusCode: response.statusCode))
        case .Failure(let error):
            guard let errorMessage = error as? CustomStringConvertible else {
                return .Failure(.GenericError)
            }
            return .Failure(.APIError(message: errorMessage.description))
        }
    }
    
}