//
//  AppDelegate.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 17/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    // It is responsible for the app's correct behavior, so it CAN'T be nil
    private var appCoordinator: AppCoordinator!
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        
        let rootController = RootViewController()
        window?.rootViewController = rootController
        window?.makeKeyAndVisible()
        
        // Setup the configuration of the OAuth parameters we'll need
        let oauthConfiguration = defaultOauthConfiguration()
        let sessionManager = OAuthManager(configuration: oauthConfiguration)
        appCoordinator = AppCoordinator(rootController: rootController, sessionManager: sessionManager)
        appCoordinator.start()
        
        AppTheme.applyTheme()
        
        return true
    }
    
    func application(app: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool {
        // The app delegate is as dumb as possible, the coordinator will handle this
        return appCoordinator.handleURL(url)
    }
    
    private func defaultOauthConfiguration() -> OAuthConfiguration {
        let oauthConfiguration = OAuthConfiguration(
            clientId: "b064ac4a89150e1090d551e92ddc19048e483209fa8c27f0175bba98fa5f7c31",
            clientSecret: "775f12fae57ffff40d8e8aa8f6642ee346095854123c405f9af8a9910b69f99e",
            redirectURI: "rbpt://oauth",
            oauthURL: "https://redbooth.com/oauth2/authorize?client_id=b064ac4a89150e1090d551e92ddc19048e483209fa8c27f0175bba98fa5f7c31&redirect_uri=rbpt%3A%2F%2Foauth&response_type=code"
        )
        return oauthConfiguration
    }
    
}
