//
//  AppCoordinator.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 17/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import UIKit

/**
 *  Responsible for the different flows of the app 
 */
class AppCoordinator {
    
    // Mark: Dependencies
    private weak var rootController: RootViewController?
    private let sessionManager: SessionManagerType
    
    // MARK: Data structures
    private let childCoordinators: NSMutableArray = []
    
    /**
     Initialization method for the App Coordinator
     
     - parameter rootController:     the app's main controller
     - parameter oauthManager:       the session manager, responsible for handling the session with the server.
     If none is provided it'll use the default one (currently pointing to the Redbooth registered app)
     */
    init(rootController: RootViewController, sessionManager: SessionManagerType) {
        self.rootController = rootController
        self.sessionManager = sessionManager
    }
    
    // MARK: Public API
    func start() {
        if isSessionValid() {
            showPomodoro()
        }
        else {
            showAuthentication()
        }
    }
    
    /**
     *  Tries to handle a given URL and returns true if successful, false otherwise
     */
    func handleURL(url: NSURL) -> Bool {
        if sessionManager.canHandleURL(url) {
            rootController?.showLoadingViewWithMessage(NSLocalizedString("Authenticating", comment: ""))
            sessionManager.handleURL(url) { [weak rootController] error in
                if let error = error {
                    rootController?.showErrorWithMessage(error.description)
                }
                else {
                    rootController?.hideLoadingView()
                }
            }
            return true
        }
        return false
    }
    
    // MARK: Private API
    private func isSessionValid() -> Bool {
        return sessionManager.didFinishSetup()
    }
    
    private func showPomodoro() {
        guard let rootController = rootController else {
            fatalError("rootController can't be nil")
        }        
        let pomodoroCoordinator = PomodoroCoordinator(rootController: rootController, sessionManager: sessionManager)
        pomodoroCoordinator.delegate = self
        pomodoroCoordinator.start()
        self.childCoordinators.addObject(pomodoroCoordinator)
    }
    
    private func showAuthentication() {
        guard let rootController = rootController else {
            fatalError("rootController can't be nil")
        }
        let authenticationCoordinator = AuthenticationCoordinator(rootController: rootController, sessionManager: sessionManager)
        authenticationCoordinator.delegate = self
        authenticationCoordinator.start()
        self.childCoordinators.addObject(authenticationCoordinator)
    }
    
}

extension AppCoordinator: AuthenticationCoordinatorDelegate {
    
    func coordinatorDidAuthenticate(coordinator: AuthenticationCoordinator) {
        childCoordinators.removeObject(coordinator)
        showPomodoro()
    }
    
}

extension AppCoordinator: PomodoroCoordinatorDelegate {
    
    func coordinatorDidCloseSession(coordinator: PomodoroCoordinator) {
        childCoordinators.removeObject(coordinator)
        showAuthentication()
    }
    
}
