//
//  AppTheme.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 17/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import UIKit

/**
 *  This struct's main goal is to provide cohesion to the UI
 */
struct AppTheme {
    
    static func applyTheme() {
        UIView.appearance().tintColor = Color.tintColor
        UINavigationBar.appearance().tintColor = Color.navigationBarText
        UINavigationBar.appearance().titleTextAttributes =
            [
                NSForegroundColorAttributeName : Color.navigationBarText,
                NSFontAttributeName : Font.content
        ]
        UINavigationBar.appearance().barTintColor = Color.tintColor
    }
    
    struct Color {
        private struct ThemeColors {
            static let darkRed = UIColor(red: 148.0/255.0, green: 17.0/255.0, blue: 0, alpha: 1)
            static let darkGreen = UIColor(red: 0, green: 128.0/255.0, blue: 64.0/255.0, alpha: 1)
            static let darkBlue = UIColor(red: 0, green: 17.0/255.0, blue: 148.0/255.0, alpha: 1)
            static let white = UIColor.whiteColor()
            static let darkGray = UIColor.darkGrayColor()
        }
        
        static let tintColor = ThemeColors.darkRed
        static let background = ThemeColors.white
        static let content = ThemeColors.darkGray
        static let buttonBackground = ThemeColors.darkRed
        static let buttonTitle = ThemeColors.white
        static let circleColorFocus = ThemeColors.darkRed
        static let circleColorBreak = ThemeColors.darkBlue
        static let circleColorLongBreak = ThemeColors.darkGreen
        static let navigationBarText = ThemeColors.white
    }
    
    struct Font {
        static let title = UIFont.boldSystemFontOfSize(22)
        static let content = UIFont.systemFontOfSize(15)
        static let button = UIFont.systemFontOfSize(17)
        static let time =  Font.monospacedNumericFontVariation(UIFont.systemFontOfSize(48))
        
        private static func monospacedNumericFontVariation(font: UIFont) -> UIFont {
            let fontDescriptor = font.fontDescriptor()
            let specialNumbersFont = fontDescriptor.fontDescriptorByAddingAttributes(
                [
                    UIFontDescriptorFeatureSettingsAttribute: [
                        [
                            UIFontFeatureTypeIdentifierKey: kStylisticAlternativesType,
                            UIFontFeatureSelectorIdentifierKey: kStylisticAltOneOnSelector,
                        ],
                        [
                            UIFontFeatureTypeIdentifierKey: kNumberSpacingType,
                            UIFontFeatureSelectorIdentifierKey: kMonospacedNumbersSelector
                        ]
                    ]
                ])
            return UIFont(descriptor: specialNumbersFont, size: font.pointSize)
        }
        
    }
    
}
