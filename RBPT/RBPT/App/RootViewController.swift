//
//  RootViewController.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 17/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import UIKit
import Cartography
import JGProgressHUD

/// Base view controller for our app
class RootViewController: UIViewController {
    
    // MARK: Constants
    private let loadingView = JGProgressHUD(style: .Dark)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = AppTheme.Color.background
    }
    
    // MARK: Public API
    /// The view controller that will be presented as root
    weak var contentViewController: UIViewController? {
        /**
        *   When setting an new root VC we must add its view to the root VC's
        *   view and make sure it will be properly scaled on every screen size.
        *   If an old VC exists, do a fadeIn/Out animation so it looks smoother.
        */
        didSet {
            guard let contentViewController = contentViewController else {
                return
            }
            
            if let fromView = oldValue?.view {
                contentViewController.view.alpha = 0
                UIView.animateWithDuration(0.4,
                    animations: { _ in
                        fromView.alpha = 0
                        contentViewController.view.alpha = 1
                    },
                    completion: { finished in
                        if finished {
                            fromView.removeFromSuperview()
                            oldValue?.willMoveToParentViewController(nil)
                            oldValue?.removeFromParentViewController()
                        }
                    }
                )
            }
            addChildViewController(contentViewController)
            view.addSubview(contentViewController.view)
            constrain(view, contentViewController.view) { superview, view in
                view.top == superview.top
                view.leading == superview.leading
                view.trailing == superview.trailing
                view.bottom == superview.bottom
            }
            didMoveToParentViewController(contentViewController)
        }
    }
    
    /**
     *  Shows a loading UI with the provided message
     */
    func showLoadingViewWithMessage(message: String) {
        loadingView.textLabel.text = message
        loadingView.showInView(view, animated: true)
    }
    
    /**
     *  Hides a loading view
     */
    func hideLoadingView() {
        loadingView.dismiss()
    }
    
    /**
     *  Shows an alert with the provided message and title and a Done button
     */
    func showMessage(message: String, withTitle title: String?) {
        let alert = UIAlertController(title: title,
            message: message, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Done", comment: ""), style: .Cancel) { [weak alert] _ in
            alert?.dismissViewControllerAnimated(true, completion: nil)
            })
        presentViewController(alert, animated: true, completion: nil)
    }
    
    /**
     *  Shows an error alert with the provided message and a Done button
     */
    func showErrorWithMessage(message: String) {
        showMessage(message, withTitle: NSLocalizedString("Error", comment: ""))
    }
    
}
