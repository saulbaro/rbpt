//
//  PomodoroConfiguration.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 23/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation

struct PomodoroConfiguration {
    
    let focusDuration: NSTimeInterval
    let shorBreakDuration: NSTimeInterval
    let longBreakDuration: NSTimeInterval
    let numberOfFocusSessionsForALongBreak: Int
    
    static func defaultConfiguration() -> PomodoroConfiguration {
        return PomodoroConfiguration(
            focusDuration: 5,//25*60,
            shorBreakDuration: 2,//5*60,
            longBreakDuration: 3,//20*60,
            numberOfFocusSessionsForALongBreak: 4)
    }
    
}
