//
//  PomodoroManager.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 23/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation


/**
 *  Responsible for managing the Pomodoro sessions
 */
class PomodoroManager: NSObject, PomodoroManagerType {
    
    // MARK: Dependencies
    private let configuration: PomodoroConfiguration
    private var numberOfFocusSessions: Int
    private var timer: NSTimer?
    
    // MARK: Creation
    init(configuration: PomodoroConfiguration = PomodoroConfiguration.defaultConfiguration()) {
        self.configuration = configuration
        self.currentSession = PomodoroSession(kind: .Focus, duration: configuration.focusDuration)
        self.numberOfFocusSessions = 0
        super.init()
    }
    
    // MARK: Public API
    /**
    *  The current Session
    */
    private(set) var currentSession: PomodoroSession
    
    /**
     *  The delegate for the manager
     */
    weak var delegate: PomodoroManagerTypeDelegate?
    
    /**
     *  Plays the current session, by starting the timer
     */
    func play() {
        currentSession.status = .Playing
        delegate?.manager(self, sessionDidUpdate: currentSession)
        let timer = NSTimer(timeInterval: 1.0, target: self, selector: "clockTick", userInfo: nil, repeats: true)
        NSRunLoop.currentRunLoop().addTimer(timer, forMode: NSRunLoopCommonModes)
        self.timer = timer
    }
    
    /**
     *  Pauses the current session
     */
    func pause() {
        currentSession.status = .Paused
        delegate?.manager(self, sessionDidUpdate: currentSession)
        timer?.invalidate()
    }
    
    /**
     *  Resets the current session
     */
    func reset() {
        currentSession = newSessionFromCurrentSession()
        delegate?.manager(self, sessionDidUpdate: currentSession)
        timer?.invalidate()
    }
    
    /**
     *  Finishes the current session
     */
    func complete() {
        currentSession.status = .Completed
        delegate?.manager(self, sessionDidUpdate: currentSession)
        if currentSession.kind == .Focus {
            numberOfFocusSessions += 1
        }
        currentSession = nextPomodoroSession()
        delegate?.manager(self, sessionDidUpdate: currentSession)
    }
    
    // MARK: Private API
    /**
    *  Crates the next session based on the current one
    */
    private func nextPomodoroSession() -> PomodoroSession {
        if currentSession.kind != .Focus {
            return PomodoroSession(kind: .Focus, duration: configuration.focusDuration)
        }
        if numberOfFocusSessions % configuration.numberOfFocusSessionsForALongBreak == 0 {
            return PomodoroSession(kind: .LongBreak, duration: configuration.longBreakDuration)
        }
        return PomodoroSession(kind: .ShortBreak, duration: configuration.shorBreakDuration)
    }
    
    /**
     *  Crates a new session from zero with the values from the current one
     */
    private func newSessionFromCurrentSession() -> PomodoroSession {
        return PomodoroSession(kind: currentSession.kind, duration: currentSession.duration)
    }
    
    /**
     *  The selector that gets called every time the Timer fires
     *  MUST NOT be made private so it can be seen by the Obj-C runtime
     */
    func clockTick() {
        currentSession.remainingTime -= 1
        delegate?.manager(self, sessionDidUpdate: currentSession)
        guard currentSession.remainingTime != 0 else {
            timer?.invalidate()
            complete()
            return
        }
    }
    
}
