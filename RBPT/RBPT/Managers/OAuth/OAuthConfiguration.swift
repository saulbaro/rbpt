//
//  OAuthConfiguration.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 20/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation

/**
 *  A Struct that represents the parameters needed to 
 *  work with OAuth
 */
struct OAuthConfiguration {
    
    let clientId: String
    let clientSecret: String
    let redirectURI: String
    let oauthURL: String
    
    init(clientId: String, clientSecret: String, redirectURI: String, oauthURL: String) {
        self.clientId = clientId
        self.clientSecret = clientSecret
        self.redirectURI = redirectURI
        self.oauthURL = oauthURL
    }
    
}