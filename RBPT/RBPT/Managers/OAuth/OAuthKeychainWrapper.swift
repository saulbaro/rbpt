//
//  OAuthKeychainWrapper.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 18/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation

/**
 *  Wrapper on top of the Keychain to store OAuth components that conforms to
 *  the OAuthStorageProvider protocol
 */
final class OAuthKeychainWrapper: KeychainWrapper, OAuthStorageProvider {
    
    // Keys
    private let AuthorizationCodeKey = "AuthorizationCode"
    private let TokenKey = "TokenCode"
    
    init() {
        super.init(serviceName: "OAuthKeychain")
    }
    
    // MARK: Public API
    var authorizationCode: String? {
        get {
            return self[AuthorizationCodeKey]
        }
        set {
            self[AuthorizationCodeKey] = newValue
        }
    }
    
    var token: Token? {
        get {
            guard let string = fetchValueForKey(TokenKey) else {
                return nil
            }
            return Token(string)
        }
        set {
            store(newValue, forKey: TokenKey)
        }
    }
    
    override func clear() {
        authorizationCode = nil
        token = nil
    }
    
}
