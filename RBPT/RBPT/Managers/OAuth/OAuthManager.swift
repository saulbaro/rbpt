//
//  OAuthManager.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 18/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation
import Moya
import Moya_ObjectMapper
import ObjectMapper
import Result

/**
 *  The protocol an object that wants to provide storage 
 *  for the manager has to conform to
 */
protocol OAuthStorageProvider {
    var authorizationCode: String? { get set }
    var token: Token? { get set }
    
    func clear()
}

/**
 *  The object that will handle OAuth in our app
 */
class OAuthManager: SessionManagerType {
    
    typealias TokenCompletion = (Result<Token, Error>) -> ()
    
    // MARK: Dependencies
    private let configuration: OAuthConfiguration
    private let provider: MoyaProvider<OAuthService>
    private var storageProvider: OAuthStorageProvider
    
    // MARK: Creation
    /**
    Creates a new OAuth manager with the provided objects, or the default ones
    if none are provided.
    */
    init(
        configuration: OAuthConfiguration,
        provider: MoyaProvider<OAuthService> = MoyaProvider<OAuthService>(),
        storageProvider: OAuthStorageProvider = OAuthKeychainWrapper()) {
            self.configuration = configuration
            self.provider = provider
            self.storageProvider = storageProvider
    }
    
    // MARK: Public API
    /**
    The delegate for the authorization process
    */
    weak var authorizationDelegate: SessionManagerAuthorizationDelegate?
    /**
     The delegate
     */
    weak var delegate: SessionManagerDelegate?
    
    /**
     The URL that must be opened to authorize the app,
     Must be a lazy var in order to be able to reference self
     */
    private(set) lazy var authorizationURL: NSURL = {
        guard let authorizationURL = NSURL(string: self.configuration.oauthURL) else {
            fatalError("OAuth URL couldn't be created, check the provided values")
        }
        return authorizationURL
    }()
    
    /**
     Checks if the Manager has all the elements needed to work properly
     */
    func didFinishSetup() -> Bool {
        return hasAuthorizationCode && hasToken
    }
    
    /**
     Indicates if it can handle the URL based on its redirectURI
     */
    func canHandleURL(url: NSURL) -> Bool {
        return url.absoluteString.hasPrefix(configuration.redirectURI)
    }
    
    /**
     Handles a URL if possible
     */
    func handleURL(url: NSURL, completion: (error: Error?) -> ()) {
        guard canHandleURL(url) else {
            completion(error: .InvalidURL)
            return
        }
        guard let parameters = extractQueryParametersFromURL(url) else {
            completion(error: .InvalidURLParameters)
            return
        }
        
        if let authorizationCode = parameters["code"] {
            storageProvider.authorizationCode = authorizationCode
            authorizationDelegate?.managerDidAuthorize(self)
            getToken() { [weak self] result in
                guard let strongSelf = self else {
                    return
                }
                switch result {
                case .Success:
                    strongSelf.authorizationDelegate?.managerGotToken(strongSelf)
                    completion(error: nil)
                case .Failure(let error):
                    completion(error: error)
                }
            }
        }
        else if let
            _ = parameters["error"],
            message = parameters["message"] {
                completion(error: .APIError(message: message))
        }
    }
    
    /**
     Signs the requests, refreshing the Token if needed
     */
    func signRequest(request: NSURLRequest, completion: (signedRequest: NSURLRequest?) -> ()) {
        guard let token = storageProvider.token else {
            closeSession()
            completion(signedRequest: nil)
            return
        }
        guard !token.hasExpired() else {
            refreshToken(token.refreshToken) { [weak self] result in
                guard let strongSelf = self else {
                    completion(signedRequest: nil)
                    return
                }
                switch result {
                case .Success(let token):
                    completion(signedRequest: self?.signRequest(request, withToken: token))
                case .Failure:
                    self?.delegate?.managerGotKickedOut(strongSelf)
                    completion(signedRequest: nil)
                }
            }
            return
        }
        completion(signedRequest: signRequest(request, withToken: token))
    }
    
    /**
     Clears the session storage and notifies the delegate
     */
    func closeSession() {
        storageProvider.clear()
        delegate?.managerGotKickedOut(self)
    }
    
    
    // MARK: Private API
    /**
    Checks if the app has an authorization code
    */
    private var hasAuthorizationCode: Bool {
        get {
            return storageProvider.authorizationCode != nil
        }
    }
    
    /**
     Checks if the app has a Token
     */
    private var hasToken: Bool {
        get {
            return storageProvider.token != nil
        }
    }
    
    /**
     Tries to obtain a Token from the OAuth API
     */
    private func getToken(completion: TokenCompletion) {
        guard let authorizationCode = storageProvider.authorizationCode else {
            completion(.Failure(.AuthorizationCodeIsEmpty))
            return
        }
        let getTokenEndpoint = OAuthService.GetToken(
            clientId: configuration.clientId,
            clientSecret: configuration.clientSecret,
            code: authorizationCode,
            redirectURI: configuration.redirectURI
        )
        performTokenRequest(getTokenEndpoint, completion: completion)
    }
    
    /**
     Tries to refresh a Token from the OAuth API
     */
    private func refreshToken(refreshToken: String, completion: TokenCompletion) {
        guard let refreshToken = storageProvider.token?.refreshToken else {
            completion(.Failure(.TokenIsNotValid))
            return
        }
        let refreshTokenEndpoint = OAuthService.RefreshToken(
            clientId: configuration.clientId,
            clientSecret: configuration.clientSecret,
            refreshToken: refreshToken)
        performTokenRequest(refreshTokenEndpoint, completion: completion)
    }
    
    /**
     Will perform the token request, store the token if it manages to get any or pass the
     error to the caller via the completion closure.
     */
    private func performTokenRequest(tokenEndpoint: OAuthService, completion: TokenCompletion) {
        provider.performRequest(
            tokenEndpoint,
            withExpectedStatusCode: 200,
            mapper: { [weak self] response in
                guard let token = try? response.mapObject() as Token else {
                    return nil
                }
                self?.storageProvider.token = token
                return token
            },
            completion: { [weak self] result in
                self?.handleResponse(result, completion: completion)
            })
    }
    
    /**
     Adds the token to the request header
     */
    private func signRequest(request: NSURLRequest, withToken token: Token) -> NSURLRequest? {
        guard let
            signedRequest = request.mutableCopy() as? NSMutableURLRequest,
            tokenType = token.tokenType,
            accessToken = token.accessToken
            else {
                return nil
        }
        let authorization = "\(tokenType.copy().capitalizedString) \(accessToken.copy())"
        signedRequest.setValue(authorization, forHTTPHeaderField: "Authorization")
        return signedRequest as NSURLRequest
    }
    
    /**
     Extracts the query parameters from a URL as a key-value dictionary
     */
    private func extractQueryParametersFromURL(url: NSURL) -> [String : String]? {
        guard let queryArray = url.query?.componentsSeparatedByString("&") where !queryArray.isEmpty else {
            return nil
        }
        let queryDictionary: [String : String] = queryArray
            .map { $0.componentsSeparatedByString("=") }
            .reduce([String : String]()) { (var dictionary, pair) in
                if let key = pair.first, value = pair.last {
                    dictionary[key] = value
                }
                return dictionary
        }
        return queryDictionary
    }
    
    /**
     *  Handles the response and kicks the user out if it got a 401 error
     */
    private func handleResponse<T>(result: Result<T, Error>, completion: (Result<T, Error>) -> ()) {
        guard case .Failure(.Unauthorized) = result else {
            completion(result)
            return
        }
        closeSession()
    }
    
}
