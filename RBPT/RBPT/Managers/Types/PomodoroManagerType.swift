//
//  PomodoroManagerType.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 23/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation

protocol PomodoroManagerTypeDelegate: class {
    func manager(manager: PomodoroManagerType, sessionDidUpdate session: PomodoroSession)
}

protocol PomodoroManagerType: class {
    
    var delegate: PomodoroManagerTypeDelegate? { get set }
    var currentSession: PomodoroSession { get }
    
    func play()
    func pause()
    func reset()
    func complete()
    
}
