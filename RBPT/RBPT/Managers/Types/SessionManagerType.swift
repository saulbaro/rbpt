//
//  SessionManager.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 20/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation

/**
 *  This Type has two delegates since marking them as optional
 *  can't be done because we are using Error, which is an enum,
 *  and can't be used in Obj-C
 */
protocol SessionManagerAuthorizationDelegate: class {
    func managerDidAuthorize(manager: SessionManagerType)
    func managerGotToken(manager: SessionManagerType)
}

protocol SessionManagerDelegate: class {
    func managerGotKickedOut(manager: SessionManagerType)
}

protocol SessionManagerType: class {
    
    var authorizationDelegate: SessionManagerAuthorizationDelegate? { get set }
    var delegate: SessionManagerDelegate? { get set }
    var authorizationURL: NSURL { get }
    
    func didFinishSetup() -> Bool
    func canHandleURL(url: NSURL) -> Bool
    func handleURL(url: NSURL, completion: (error: Error?) -> ())
    func signRequest(request: NSURLRequest, completion: (signedRequest: NSURLRequest?) -> ())
    func closeSession()
    
}
