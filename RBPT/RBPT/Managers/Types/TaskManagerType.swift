//
//  TaskManager.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 20/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation
import Result

protocol TaskManagerType: class {
    
    func fetchOpenTasksAssignedToUserGroupedByFuzzyDate(completion: (Result<[(String, [Task])], Error>) -> ())
    func addTime(time: NSTimeInterval, toTask task: Task, completion: (Result<Void, Error>) -> ())
    func closeTask(task: Task, completion: (Result<Void, Error>) -> ())
    func clearManager()
    
}
