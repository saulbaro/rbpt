//
//  Task+FuzzyDate.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 23/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation

/**
*  Date formatter that converts a date with the DueOn format to an NSDate
*   declared as a singleton
*/
private let dateFormatter: NSDateFormatter = {
    let formatter = NSDateFormatter()
    formatter.dateFormat = "YYYY-MM-DD"
    return formatter
}()

/**
 *  Extension on Task to handle fuzzy Dates
 */
extension Task {
    
    enum FuzzyDate: Int {
        case Today = 0
        case DueSoon
        case FarFuture
        case NoDueDate
        
        var title: String {
            switch self {
            case .Today:
                return NSLocalizedString("Today", comment: "")
            case .DueSoon:
                return NSLocalizedString("Due soon", comment: "")
            case .FarFuture:
                return NSLocalizedString("Far future", comment: "")
            case .NoDueDate:
                return NSLocalizedString("No due date", comment: "")
            }
        }
    }
    
    var dueOnDate: NSDate? {
        get {
            guard let dueOn = dueOn else { return nil }
            return dateFormatter.dateFromString(dueOn)
        }
    }
    
    /**
     *  Converts an NSDate to a fuzzy date
     */
    var fuzzyDate: FuzzyDate {
        // Urgent tasks are categorized as Today tasks
        if self.isUrgent == true { return .Today }
        // No due date
        guard let dueOnDate = self.dueOnDate else { return .NoDueDate }
        // Overdue tasks and Today tasks are categorized as Today tasks
        let today = NSDate()
        if self.isUrgent == true
            || dueOnDate.compare(today) == .OrderedAscending
            || NSCalendar.currentCalendar().isDateInToday(dueOnDate) {
                return .Today
        }
        guard let nearFuture = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: 7, toDate: today, options: .WrapComponents) else {
            return .NoDueDate
        }
        if dueOnDate.compare(nearFuture) == .OrderedAscending
            || NSCalendar.currentCalendar().isDate(dueOnDate, inSameDayAsDate: nearFuture) {
                return .DueSoon
        }
        return .FarFuture
    }
    
    /**
     *  Sorts two tasks so the one with the most recent date goes first.
     *  Tasks with no due dates get ordered later
     */
    static func sortByDueOn(lhs: Task, rhs: Task) -> Bool {
        guard let lhsDueOn = lhs.dueOnDate else { return false }
        guard let rhsDueOn = rhs.dueOnDate else { return true }
        return lhsDueOn.compare(rhsDueOn) == .OrderedAscending
    }
    
}
