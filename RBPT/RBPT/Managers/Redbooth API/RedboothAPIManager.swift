//
//  RedboothAPIManager.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 18/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
import Moya_ObjectMapper
import Result


/**
 *  The protocol an object that wants to provide storage
 *  for the manager has to conform to
 */
protocol RedboothAPIStorageProvider {
    var user: User? { get set }
    func clear()
}

/**
 *  Responsible for managing the data required by the app from the Redbooth API
 */
class RedboothAPIManager: TaskManagerType {
    
    // MARK: Dependencies
    private weak var sessionManager: SessionManagerType?
    private let provider: MoyaProvider<RedboothAPI>
    private var storageProvider: RedboothAPIStorageProvider
    
    // MARK: Creation
    init(
        sessionManager: SessionManagerType,
        storageProvider: RedboothAPIStorageProvider = RedboothAPIKeychainWrapper()
        ) {
            self.sessionManager = sessionManager
            self.provider = RedboothAPIManager.createProviderWithSessionManager(sessionManager)
            self.storageProvider = storageProvider
    }
    
    // MARK: Public API
    /**
    *  Gets all the open tasks assigned to a user and groups them by Due date
    */
    func fetchOpenTasksAssignedToUserGroupedByFuzzyDate(completion: (Result<[(String, [Task])], Error>) -> ()) {
        getUserInfo { [weak self] result in
            switch result {
            case .Success(let user):
                let endpoint = RedboothAPI.GetTasks(
                    assignedUserIdentifier: user.identifier,
                    status: Task.Status.Open.rawValue)
                self?.provider.performRequest(endpoint,
                    withExpectedStatusCode: 200,
                    mapper: { response in
                        guard let tasks = try? response.mapArray() as [Task] else { return nil }
                        return self?.groupTasksByDueDate(tasks)
                    },
                    completion: { [weak self] (result: Result<[(String, [Task])], Error>) in
                        self?.handleResponse(result, completion: completion)
                    }
                )
            case .Failure(let error):
                completion(.Failure(error))
            }
        }
    }
    
    /**
     *  Adds time to a task on the backend
     */
    func addTime(time: NSTimeInterval, toTask task: Task, completion: (Result<Void, Error>) -> ()) {
        let minutes = Int(time) / 60
        print("Time \(minutes)")
        let endpoint = RedboothAPI.CreateComment(
            targetType: Task.targetType,
            targetIdentifier: task.identifier,
            minutes: minutes)
        provider.performRequest(endpoint,
            withExpectedStatusCode: 201,
            mapper: { _ in return },
            completion: { [weak self] result in
                self?.handleResponse(result, completion: completion)
            }
        )
    }
    
    /**
     *  Closes a task on the backend
     */
    func closeTask(task: Task, completion: (Result<Void, Error>) -> ()) {
        let endpoint = RedboothAPI.UpdateTaskStatus(
            taskIdentifier: task.identifier,
            newStatus: Task.Status.Resolved.rawValue)
        provider.performRequest(endpoint,
            withExpectedStatusCode: 200,
            mapper: { _ in return },
            completion: { [weak self] result in
                self?.handleResponse(result, completion: completion)
            }
        )
    }
    
    /**
     *  Clears the manager and it's storage
     */
    func clearManager() {
        storageProvider.clear()
    }
    
    // MARK: Private API
    /**
    *  Creates an API provider with the session manager as the responsible object to
    *  sign requests
    */
    private static func createProviderWithSessionManager(sessionManager: SessionManagerType) -> MoyaProvider<RedboothAPI> {
        let requestClosure = { (endpoint: Endpoint<RedboothAPI>, done: NSURLRequest -> Void) in
            let request = endpoint.urlRequest
            sessionManager.signRequest(request, completion: { signedRequest in
                guard let signedRequest = signedRequest else {
                    return
                }
                done(signedRequest)
            })
        }
        return MoyaProvider(requestClosure: requestClosure)
    }
    
    /**
     *  Gets the logged user information from local storage if previously cached
     *  or from the API otherwise
     */
    private func getUserInfo(completion: (Result<User, Error>) -> ()) {
        if let user = storageProvider.user {
            completion(.Success(user))
            return
        }
        let endpoint = RedboothAPI.GetUserInfo
        provider.performRequest(endpoint,
            withExpectedStatusCode: 200,
            mapper: { [weak self] response in
                let user = try? response.mapObject() as User
                self?.storageProvider.user = user
                return user
            },
            completion: { [weak self] result in
                self?.handleResponse(result, completion: completion)
            }
        )
    }
    
    /**
     *  Handles the response and kicks the user out if it got a 401 error
     */
    private func handleResponse<T>(result: Result<T, Error>, completion: (Result<T, Error>) -> ()) {
        guard case .Failure(.Unauthorized) = result else {
            completion(result)
            return
        }
        clearManager()
        sessionManager?.closeSession()
    }
    
    /**
     *  Groups a Task array into an array of tuples String-[Task] where the
     *  first value indicates the dueOn value and the second value is the
     *  list of taks that should be done on that date
     */
    private func groupTasksByDueDate(tasks: [Task]) -> [(String, [Task])] {
        let tasksOrderedByDueOn = tasks.sort(Task.sortByDueOn)
        var groupedTasks: [Task.FuzzyDate : [Task]] = [:]
        tasksOrderedByDueOn.forEach { task in
            let key = task.fuzzyDate
            if groupedTasks[key] == nil { groupedTasks[key] = [] }
            groupedTasks[key]?.append(task)
        }
        return groupedTasks.sort { $0.0.0.rawValue < $0.1.0.rawValue }.map { ($0.0.title , $0.1) }
    }
    
}
