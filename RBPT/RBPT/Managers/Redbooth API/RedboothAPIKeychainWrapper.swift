//
//  RedboothAPIKeychainWrapper.swift
//  RBPT
//
//  Created by Saül Baró Ruiz on 21/01/16.
//  Copyright © 2016 Saül Baró Ruiz. All rights reserved.
//

import Foundation

/**
 *  Wrapper on top of the Keychain to store API components that conforms to
 *  the RedboothAPIStorageProvider protocol
 */
final class RedboothAPIKeychainWrapper: KeychainWrapper, RedboothAPIStorageProvider {
    
    // Keys
    private let UserKey = "User"
    
    init() {
        super.init(serviceName: "RedboothAPI")
    }
    
    // MARK: Public API
    var user: User? {
        get {
            guard let string = fetchValueForKey(UserKey) else {
                return nil
            }
            return User(string)
        }
        set {
            store(newValue, forKey: UserKey)
        }
    }
    
    override func clear() {
        user = nil
    }
    
}
